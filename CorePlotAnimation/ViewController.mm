//
//  ViewController.m
//  CorePlotAnimation
//
//  Created by Hiromasa OHNO on 2014/04/02.
//  Copyright (c) 2014年 cflat. All rights reserved.
//

#import "ViewController.h"

#import <opencv2/opencv.hpp>
#import <opencv2/highgui/ios.h>
#import <vector>

std::vector< std::vector<cv::KeyPoint> > _plots;
NSUInteger _currentIndex;

@interface ViewController()
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) CPTGraph* graph;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    _currentIndex = 0;
    
    // あらかじめ画像を読み込んで特徴量の検出を済ませておく
    [self loadFrames];
    
    // グラフ描画の設定
    UIView* graphView = [self createGraphView];
    [self.view addSubview:graphView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadFrames
{
    for(NSUInteger i=1; i<=100; ++i){
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"frame%03d.jpg", i]];
        cv::Mat mat;
        UIImageToMat(image, mat);
        
        cv::FastFeatureDetector det = cv::FastFeatureDetector();
        
        std::vector<cv::KeyPoint> points;
        det.detect(mat, points);
        
        // グラフのY軸と画像のY軸が反対なので値を反転する
        for(size_t j=0; j<points.size(); ++j){
            points[j].pt.y = 360 - points[j].pt.y;
        }
        
        _plots.push_back(points);
    }
}

- (UIView*)createGraphView
{
    CGRect rect = CGRectMake(0, 100, 320, 180);
    CPTGraphHostingView *graphView = [[CPTGraphHostingView alloc] initWithFrame:rect];
    // graphView.backgroundColor = [UIColor yellowColor];
    CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:rect];
    graphView.hostedGraph = graph;
    
    CPTScatterPlot *scatterPlot = [CPTScatterPlot new];
    scatterPlot.identifier = @"ID";
    scatterPlot.dataSource = self;
    scatterPlot.dataLineStyle = nil;
    scatterPlot.plotSymbol = [CPTPlotSymbol diamondPlotSymbol];
    scatterPlot.plotSymbol.size = CGSizeMake(3, 3);
    
    CPTMutableLineStyle *symbolStyle = [CPTMutableLineStyle new];
    symbolStyle.lineColor = [CPTColor colorWithCGColor:[UIColor colorWithRed:0.0 green:0.1 blue:0.5 alpha:1.0].CGColor];
    scatterPlot.plotSymbol.lineStyle = symbolStyle;
    
    [graph addPlot:scatterPlot];
    
    CPTXYPlotSpace *space = (CPTXYPlotSpace*)graph.defaultPlotSpace;
    space.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0) length:CPTDecimalFromFloat(360)];
    space.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(0) length:CPTDecimalFromFloat(480)];
    
    graph.paddingLeft   = 0.0f;
    graph.paddingRight  = 0.0f;
    graph.paddingTop    = 0.0f;
    graph.paddingBottom = 0.0f;
    graph.axisSet = nil;
    
    self.graph = graph;
    return graphView;
}

- (NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot
{
    return _plots[_currentIndex].size();
}

- (NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)idx
{
    NSNumber *num;
    std::vector<cv::KeyPoint>& points = _plots[_currentIndex];
    switch (fieldEnum) {
        case CPTScatterPlotFieldX:
            num = [NSNumber numberWithFloat:points[idx].pt.x];
            break;
        case CPTScatterPlotFieldY:
            num = [NSNumber numberWithFloat:points[idx].pt.y];
        default:
            break;
    }
    return num;
}

- (IBAction)start:(UIButton*)sender
{
    if ([self.timer isValid] ){
        [self.timer invalidate];
        [sender setTitle:@"Start" forState:UIControlStateNormal];
    }else{
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(stepAnimation) userInfo:nil repeats:YES];
        [sender setTitle:@"Stop" forState:UIControlStateNormal];
    }
}

- (void)stepAnimation
{
    _currentIndex++;
    if( _currentIndex >= _plots.size() ){
        _currentIndex = 0;
    }
    [self.graph reloadData];
}

@end
