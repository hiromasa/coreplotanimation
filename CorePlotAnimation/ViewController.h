//
//  ViewController.h
//  CorePlotAnimation
//
//  Created by Hiromasa OHNO on 2014/04/02.
//  Copyright (c) 2014年 cflat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CorePlot/CorePlot-CocoaTouch.h>

@interface ViewController : UIViewController<CPTPlotDataSource>
- (IBAction)start:(UIButton*)sender;

@end
