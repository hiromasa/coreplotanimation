//
//  AppDelegate.h
//  CorePlotAnimation
//
//  Created by Hiromasa OHNO on 2014/04/02.
//  Copyright (c) 2014年 cflat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
