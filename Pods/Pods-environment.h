
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// CorePlot
#define COCOAPODS_POD_AVAILABLE_CorePlot
#define COCOAPODS_VERSION_MAJOR_CorePlot 1
#define COCOAPODS_VERSION_MINOR_CorePlot 5
#define COCOAPODS_VERSION_PATCH_CorePlot 1

// OpenCV
#define COCOAPODS_POD_AVAILABLE_OpenCV
#define COCOAPODS_VERSION_MAJOR_OpenCV 2
#define COCOAPODS_VERSION_MINOR_OpenCV 4
#define COCOAPODS_VERSION_PATCH_OpenCV 8

